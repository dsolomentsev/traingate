import matplotlib.pyplot as plt
from datetime import datetime
import argparse
import numpy as np
from scipy.interpolate import interp1d

# Function to convert timeLine to seconds from the file start
def timeToSec(timeLine):
    secLine = []
    for j in range(0, len(timeLine)):
        secLine.append((timeLine[j] - datetime(timeLine[j].year, timeLine[j].month, timeLine[j].day)).total_seconds())
    secLine[:] = [(i - secLine[0])/3600 for i in secLine]
    return secLine

# Function to define the moving periods 
# and bring all the velocity profiles to the unified time line template
def unifyVelocityProfiles(timeLineTemplate, speed):
    
    tSpeed = []
    k = 0
    
    # Look through the speed array
    while k != len(speed):
        j = 0
        cSpeed = []
        # In case current speed is positive - start writing next moving frame
        cSpeed.append(speed[k + j])
        while speed[k + j] != 0:
            j = j + 1
            cSpeed.append(speed[k + j])
        # If this is a true frame and not just a single noise measurement, process it
        if len(cSpeed) > 1:
            f = interp1d([float(x)/len(cSpeed) for x in range(len(cSpeed))], cSpeed, bounds_error=False, fill_value=0)
            interpolated = f(timeLineTemplate)
            zeroInd = [ii for ii, jj in enumerate(interpolated) if jj == 0]
            # If the frame is long and sensible - add it to the statistics
            if len(zeroInd) <= 4: 
                tSpeed.append(f(timeLineTemplate))
        k = k + j + 1
    return tSpeed

# Function that returns the velocity profile basing on the GPS logfile    
def getVelocityProfile(logFilename, doPlotting):

    # Played around with the argument parsing - no need when using function
    #parser = argparse.ArgumentParser(description='Plot and anlyse the GPS log')
    #parser.add_argument('-f', '--filename', type=str, help='GPS logger filename', default='20131005120800.txt')
    #ARGS = parser.parse_args()
    
    logFile = open(logFilename, 'r')
    speed = []
    timeLine = []
    latitude = []
    longitude = []
    altitude = []

    # Prepare time series
    for line in logFile:
        time,lat,lon,elev,accuracy,bearing,vel = line.split(',')
        if not 'speed' in vel[0:vel.find('\n')]:    
            speed.append(float(vel[0:vel.find('\n')]))
            timeLine.append(datetime.strptime(time, '%Y-%m-%dT%H:%M:%SZ')) #2013-10-03T17:43:05Z         
        if not 'lat' in lat: 
            latitude.append(float(lat))
        if not 'lon' in lon: 
            longitude.append(float(lon))
        if not 'elevation' in elev:
            altitude.append(float(elev))
    
    # Generate template of the unified timeline
    timeLineTemplate = [float(x)/100 for x in range(0, 100)]
    tSpeed = unifyVelocityProfiles(timeLineTemplate, speed)
    mnSpeed = np.mean(tSpeed, axis = 0)
    
    # Scale the profile by maximum velocity
    mnSpeed = mnSpeed/max(mnSpeed)
    
    # Plot the velocity profile if asked to
    if doPlotting:
        plt.plot(timeLineTemplate, mnSpeed, '-o')
        plt.hold(True)
        plt.ylabel('Spped, m/s')
        plt.xlabel('Percentage of the path between stations')
        plt.title('Unified velocity profile basing on the GPS log from ' + logFilename[logFilename.rfind('/')+1:len(logFilename)])
        plt.show()
    
    return timeLineTemplate, mnSpeed
