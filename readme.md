# ToDo - list for this project

# Shortlist
- Check out how the script works for novodachnaya station
- Tune the constants if needed
- If everything works OK, start expanding

# Gate closing issues
- Estimate the time (space?) lag for closing the gate

# Questions
- how to account for staying between stations (e.g. Mark)?
- how to find the railroad and neighbour stations by code?
- how to calculate the distance between the neighbour stations?
- need a tble for the station <-> station_code connection for tutu
- legal questions: is it good to parse tutu? 

