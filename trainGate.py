import tutu
from getVelocityProfile import getVelocityProfile
import mapTools as MT
import matplotlib.pyplot as plt
from matplotlib import dates
from scipy.integrate import trapz
from scipy.interpolate import interp1d
from datetime import datetime
from datetime import timedelta
import numpy as np

def getCloseOpenTimes(schedAtoB, distanceAtoB, distanceToCross): 

    # At which distance the gate is closed, constant
    distToClose = 1.e3        

    # Incase the time interpolation will be out of bounds
    arrivalCloseTime = 60 # sec

    # Get general train velocity profile between the stations
    timeLine, trainVel = getVelocityProfile('/home/dmitry/BitBucket/traingate/data/gpsLogs/20131005120800.txt', \
        doPlotting = False) 

    meanGeneralVel = trainVel.mean()
    timeClose = []
    timeOpen = []

    # Loop over the trains in schedule and get times they cross the road
    for i in range(len(schedAtoB)):

        totalTime = schedAtoB[i]["arrival_time"] - schedAtoB[i]["departure_time"]
        meanVel = distanceAtoB / totalTime.total_seconds()
           
        # A rough scaling procedure, but the errors are rather small (~ 1m in distance)
        velProfile = trainVel * meanVel / meanGeneralVel
        timeLineReal = [timeLine[x] * totalTime.total_seconds() for x in range(len(timeLine))]
    
        # Calculate distance passed at each time step
        distPassed = []
        for j in range(len(timeLineReal)):
            distPassed.append(trapz(velProfile[0:j], timeLineReal[0:j]))
    
        # Get the time points when the gate closes and opens given the distance vector
        # This is based on the idea that the gate is closed/open when the train is $distToClose meters away  
        interpolant = interp1d(distPassed, timeLineReal, bounds_error=False, fill_value=-arrivalCloseTime)
        secClose = interpolant(distanceToCross - distToClose)
        secOpen = interpolant(distanceToCross + distToClose)
    
        # Convert calculated points to the normal time
        timeClose.append(schedAtoB[i]["departure_time"] + timedelta(seconds = float(secClose)))
        timeOpen.append(schedAtoB[i]["departure_time"] + timedelta(seconds = float(secOpen)))
    
        #plt.plot(timeLineReal, distPassed, '-o')
        #plt.show()
        #print "closed: ", timeClose, "  ; opened: ", timeOpen
    return timeClose, timeOpen

def interlaceCloseOpenTimes(timeLine, schedAtoB, schedBtoA, distanceAtoB, distanceAtoC, distanceBtoC):
    
    timeCloseAtoB, timeOpenAtoB = getCloseOpenTimes(schedAtoB, distanceAtoB, distanceAtoC)
    timeCloseBtoA, timeOpenBtoA = getCloseOpenTimes(schedBtoA, distanceAtoB, distanceBtoC)
    
    theGateIsClosed = []
    trainCountAtoB = 0
    trainCountBtoA = 0
    
    for t in range(len(timeLine)):

        closesAtoB = (timeOpenAtoB[trainCountAtoB] > timeLine[t]) & (timeCloseAtoB[trainCountAtoB] < timeLine[t])
        closesBtoA = (timeOpenBtoA[trainCountBtoA] > timeLine[t]) & (timeCloseBtoA[trainCountBtoA] < timeLine[t])
        
        if timeLine[t] > timeOpenAtoB[trainCountAtoB]:
            trainCountAtoB = min(trainCountAtoB + 1, len(timeOpenAtoB) - 1)
            
        if timeLine[t] > timeOpenBtoA[trainCountBtoA]:
            trainCountBtoA = min(trainCountBtoA + 1, len(timeOpenBtoA) - 1)
        
        if(closesAtoB | closesBtoA):
            theGateIsClosed.append(1)
        else:
            theGateIsClosed.append(0)
        
    return theGateIsClosed        

#===============================================================================
# Main body
#===============================================================================


# Some inputs are hardcoded up to the moment
# e.g. station codes
codeStA = 29404
codeStB = 29104

# Select the date
#theDate = datetime.now()
theDate = datetime(2013, 10, 21, 21, 27, 0) 
#theDate = datetime(datetime.now().year, datetime.now().month, datetime.now().day) 
#theDate = datetime(2013, 10, 16)

# Schedule is a tuple with named fields
# The stations in TuTu system have associated codes
# Known issues:
# the stations within one line are separated by 100
schedAtoB = tutu.main(dsc = codeStA, asc = codeStB, dateOfSchedule = theDate.strftime("%Y.%m.%d"));
schedBtoA = tutu.main(dsc = codeStB, asc = codeStA, dateOfSchedule = theDate.strftime("%Y.%m.%d"));

# Get distance betweeen stations and minimum distance to crossing
distanceAtoB, distanceAtoC, distanceBtoC = MT.getStationsDistance('/home/dmitry/BitBucket/traingate/data/osm/map.osm')

# Calculate the timeline for the chosen date
timeLine = []
for t in range(24*3600):
        timeLine.append(theDate + timedelta(seconds = float(t)))

# Get open and close times of the gate
theGateIsClosed = interlaceCloseOpenTimes(timeLine, schedAtoB, schedBtoA, distanceAtoB, distanceAtoC, distanceBtoC)

# Plot the closing periods 1 hour before and 1 hour after the current time
hourBefore = theDate - timedelta(seconds = float(3600))
hourAfter = theDate + timedelta(seconds = float(3600))

pltTime = []
pltClosed = []
for t in range(len(timeLine)):
    if ((timeLine[t] > hourBefore) & (timeLine[t] < hourAfter)):
        pltTime.append(timeLine[t])
        pltClosed.append(theGateIsClosed[t])
        
hfmt = dates.DateFormatter('%d.%m %H:%M')
fig = plt.figure()
ax = fig.add_subplot(111)
cp = ax.plot(dates.date2num(pltTime), pltClosed, '-o')
plt.yticks([0, 1], ['Open', 'Closed'])
ax.xaxis.set_major_locator(dates.AutoDateLocator())
ax.xaxis.set_major_formatter(hfmt)
plt.xticks(rotation='vertical')
plt.subplots_adjust(bottom=.3)
plt.show()
