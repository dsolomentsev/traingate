import xml.etree.ElementTree as ET
# The set of functions to deal with OSM

# The function to get stations and barriers coordinates from the OSM file
def getStationsAndBarriers(mapFilename):

    # Define vars
    nodes = []
    stationsName = []
    stationsLon = []
    stationsLat = []
    barriersLon = []
    barriersLat = []

    tree = ET.parse(mapFilename)
    root = tree.getroot()

    # Fill stations coordinates
    for node in root.findall("./node"):
        if len(node.findall("./tag[@k='railway'][@v='station']")) == 1:
            nodes.append(node)
            if len(node.findall("./tag[@k='name']")) == 1:
                stationsName.append(node.find("./tag[@k='name']").attrib['v'])
    
    for node in nodes :
        if not float(node.attrib['lon']) in stationsLon :
            stationsLon.append(float(node.attrib['lon']))
        if not float(node.attrib['lon']) in stationsLat :
            stationsLat.append(float(node.attrib['lat']))
    
    # Fill barriers coordinates
    for node in root.findall("./node"):
        if len(node.findall("./tag[@k='railway'][@v='level_crossing']")) == 1:
                nodes.append(node)
    for node in nodes :
        if not float(node.attrib['lon']) in barriersLon :
            barriersLon.append(float(node.attrib['lon']))
        if not float(node.attrib['lat']) in barriersLat :
            barriersLat.append(float(node.attrib['lat']))
        
    return stationsLon, stationsLat, barriersLon, barriersLat
    
    
# Convert geo degrees to meters (lousy way - sphere approximation)
def getXYpos(relativeNullPointX, relativeNullPointY, latitude, longitude):
    deltaLatitude = latitude - relativeNullPointY
    deltaLongitude = longitude - relativeNullPointX
    latitudeCircumference = 40075160 * math.cos(relativeNullPointY)
    resultX = deltaLongitude * latitudeCircumference / 360
    resultY = deltaLatitude * 40008000 / 360
    return resultX, resultY
    

def getStationsDistance(mapFileName):
    # Get stations and barrier coordinates    
    stationsLon, stationsLat, barriersLon, barriersLat = getStationsAndBarriers(mapFileName)    

    # Calculate distances between stations A and B and crossing - C
    # Current example - Lianozovo and Novodachnaya
    distanceAtoB = 5.26 * 1e3
    distanceAtoC = 1.8 * 1e3
    distanceBtoC = distanceAtoB - distanceAtoC

    # Choose minimum to reduce the errors from the velocity profile
    return distanceAtoB, distanceAtoC, distanceBtoC
    

    

